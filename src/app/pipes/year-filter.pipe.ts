import { Pipe, PipeTransform } from '@angular/core';
import { Movie } from '../models/movie';

@Pipe({
  name: 'yearFilter'
})
export class YearFilterPipe implements PipeTransform {

  transform(movies: Array<Movie>, yearFrom: number, yearTo: number): any {
    return movies
      .filter(x => parseInt(x.Year) >= yearFrom && parseInt(x.Year) <= yearTo);
  }

}
