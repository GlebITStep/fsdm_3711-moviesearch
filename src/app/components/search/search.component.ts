import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { OmdbApiResponse } from 'src/app/models/omdb-api-response';
import { Movie } from 'src/app/models/movie';
import { OmdbApiService } from 'src/app/services/omdb-api.service';
import { FavoritesStorageService } from 'src/app/services/favorites-storage.service';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss']
})
export class SearchComponent implements OnInit {
  title = '';
  movies = Array<Movie>();
  type = '';
  yearFrom = 1888;
  yearTo = 2019;
  page = 1;
  totalResults = 0;

  constructor(
    private omdbApi: OmdbApiService,
    private favoritesStorage: FavoritesStorageService) { }

  ngOnInit() {
  }

  async onSearch() {
    this.page = 1;
    await this.loadMovies();
  }

  // onLike(movie: Movie) {
  //   if (movie.IsFavorite == undefined) {
  //     movie.IsFavorite = true;
  //   } else {
  //     movie.IsFavorite = !movie.IsFavorite;   
  //   } 
  //   this.favoritesStorage.addMovie(movie);
  // }

  async onLoadMoreClick() {
    this.page++;
    await this.loadMovies();
  }

  async loadMovies() {
    let response = await this.omdbApi.searchMovies(this.title, this.page);
    if(response.Response) { 
      this.totalResults = parseInt(response.totalResults);
      this.movies = this.movies.concat(response.Search);
    }
  }
}