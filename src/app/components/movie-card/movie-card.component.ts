import { Component, Input, Output, EventEmitter } from '@angular/core';
import { Movie } from 'src/app/models/movie';
import { FavoritesStorageService } from 'src/app/services/favorites-storage.service';

@Component({
  selector: 'app-movie-card',
  templateUrl: './movie-card.component.html',
  styleUrls: ['./movie-card.component.scss']
})
export class MovieCardComponent {
  @Input() movie: Movie;
  // @Output() like = new EventEmitter<Movie>();

  constructor(private favoritesStorage: FavoritesStorageService) { }

  onImageLoadError() {
    this.movie.Poster = '../../../assets/placeholder.jpg'; 
  }

  onLikeClick() {
    //this.like.emit(this.movie);
    
    if (this.movie.IsFavorite == undefined) {
      this.movie.IsFavorite = true;
      this.favoritesStorage.addMovie(this.movie); 
    } else if (this.movie.IsFavorite) {
      this.movie.IsFavorite = false;
      this.favoritesStorage.removeMovie(this.movie); 
    } else {
      this.movie.IsFavorite = true;
      this.favoritesStorage.addMovie(this.movie); 
    }
  }
}
