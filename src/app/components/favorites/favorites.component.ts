import { Component, OnInit } from '@angular/core';
import { Movie } from 'src/app/models/movie';
import { FavoritesStorageService } from 'src/app/services/favorites-storage.service';

@Component({
  selector: 'app-favorites',
  templateUrl: './favorites.component.html',
  styleUrls: ['./favorites.component.scss']
})
export class FavoritesComponent implements OnInit {
  movies: Array<Movie>;

  constructor(private favoritesStorage: FavoritesStorageService) { }

  ngOnInit() {
    this.movies = this.favoritesStorage.getAllFavorites();
  }

}
