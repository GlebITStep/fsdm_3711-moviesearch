import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Movie } from 'src/app/models/movie';
import { HttpClient } from '@angular/common/http';
import { OmdbApiService } from 'src/app/services/omdb-api.service';

@Component({
  selector: 'app-movie-details',
  templateUrl: './movie-details.component.html',
  styleUrls: ['./movie-details.component.scss']
})
export class MovieDetailsComponent implements OnInit {
  id: string;
  movie: Movie;

  constructor(
    private route: ActivatedRoute,
    private omdbApi: OmdbApiService) { }

  async ngOnInit() {
    this.id = this.route.snapshot.paramMap.get('id');
    this.movie = await this.omdbApi.getMovieDetails(this.id);
  }

}
