import { Injectable } from '@angular/core';
import { Movie } from '../models/movie';

@Injectable({
  providedIn: 'root'
})
export class FavoritesStorageService { 
  
  addMovie(movie: Movie): void {
    let movies: Array<Movie>;
    let moviesJson = localStorage.getItem('favorites');
    if (moviesJson) {
      movies = JSON.parse(moviesJson);
    } else {
      movies = new Array<Movie>();
    }
    movies.push(movie);
    localStorage.setItem('favorites', JSON.stringify(movies));
  }

  removeMovie(movie: Movie): void {
    let movies: Array<Movie>;
    let moviesJson = localStorage.getItem('favorites');
    if (moviesJson) {
      movies = JSON.parse(moviesJson);
      movies = movies.filter(x => x.imdbID != movie.imdbID);
      localStorage.setItem('favorites', JSON.stringify(movies));
    }
  }

  getAllFavorites(): Array<Movie> {
    let movies: Array<Movie>;
    let moviesJson = localStorage.getItem('favorites');
    if (moviesJson) {
      movies = JSON.parse(moviesJson);
    } else {
      movies = new Array<Movie>();
    }
    return movies;
  }
}
